This is a 3D Earth viewer.

![](assets/demo/global_view.png){width=50%}

It is multi scale, being able to go from space to high zoom.

![](assets/demo/high_zoom.png){width=50%}

It can display vector data as well.

![](assets/demo/vector_data.png){width=50%}

What is displayed can be highly customized through the [config.json](config.json) file (set your own texture cache path !). 


# Idées

Lorsqu'on arrive assez près de la surface, on peut commencer à utiliser des modèles 3d pour les bâtiments, les arbres, les véhicules. Puis des shaders pour les surfaces d'eau. Enfin, on aurait un monde virtuel généré procéduralement mais de manière non aléatoire, pour respecter les caractéristiques des images satellites.

# Bibliothèques

Cinder
OpenVR
libpng

# Carte du ciel et des constellations

https://svs.gsfc.nasa.gov/4851

# Tuiles mercator png

https://c.tile.openstreetmap.org/{zoom_level}/{latitude_id}/{longitude_id}.png
https://c.tile.opentopomap.org/{zoom_level}/{latitude_id}/{longitude_id}.png
-> Ne pas utiliser directement dans le config.json ! (https://operations.osmfoundation.org/policies/tiles/)


# Tuiles satellites diverses

https://earthexplorer.usgs.gov/
https://search.earthdata.nasa.gov

# Données vectorielles

http://www.naturalearthdata.com/downloads/

## OpenStreetMap
https://download.geofabrik.de (récursif, les feuilles ont les .shp)
https://download.openstreetmap.fr/extracts/europe/
https://wiki.openstreetmap.org/wiki/Osmfilter#Object_Filter


Pour plus de sources, voir :
https://gisgeography.com/free-global-dem-data-sources/
https://gisgeography.com/best-free-gis-data-sources-raster-vector/
https://eos.com/blog/free-satellite-imagery-sources/

