#include"cinder-vr/cinder_vr_app.h"
#include"cinder_app.h"
#include"utils/math.h"

#include"cinder/app/RendererGl.h"

namespace cinder::app{
	float get_far_distance(CinderVrApp<godefv::planet3d::cinder_app> const& app){
		return 200000.f;
	}
	ci::mat4 get_vr_space_to_world_matrix(CinderVrApp<godefv::planet3d::cinder_app> const& app){
		return app.spectator_camera.getInverseViewMatrix();
	}
	void pre_draw(CinderVrApp<godefv::planet3d::cinder_app> const& app){
		ci::vec3 spherical_coordinates=make_spherical_coordinates(gl::calcViewMatrixInverse()[3]);
		spherical_coordinates.z-=app.assets.planet.radius;
		app.assets.planet.shader->uniform("view_coordinates", spherical_coordinates);
	}
}

//CINDER_APP( godefv::planet3d::cinder_app, ci::app::RendererGl )
CINDER_APP( CinderVrApp<godefv::planet3d::cinder_app>, ci::app::RendererGl )


