#include"database.h"

#include"../../utils/cinder/json.h"
#include"../../utils/string.h"
#include"cinder/app/App.h"
#include <iterator>
#include <utility>

namespace godefv::planet3d{

std::variant<shapefile_point_t,shapefile_line_t,shapefile_polygon_t> vector_database_t::operator[](vector_database_index_t const& index) const{
	switch(index.type){
	case vector_data_type::point  : return this->point_data  [index.source_index].data[index.data_index];
	case vector_data_type::line   : return this->line_data   [index.source_index].data[index.data_index];
	case vector_data_type::polygon: return this->polygon_data[index.source_index].data[index.data_index];
	default: return{};
	}
}

std::vector<vector_database_index_t> vector_database_t::search(std::string const& query_string) const{
	std::vector<vector_database_index_t> result;
	for(auto key:split(to_lower(query_string))){
		replace(key, ",", "");
		auto key_result=this->indices.find(key);
		if(key_result==this->indices.end()) continue;
		if(result.empty()){
			std::ranges::copy(key_result->second, std::back_inserter(result));
		}else{
			std::ranges::set_intersection(std::exchange(result, {}), key_result->second, std::back_inserter(result));
		}
	}
	return result;
}

godefv::return_value_and_status_t<vector_data_display_t> to_vector_data_display(std::string const& s){
	if(s=="always") return vector_data_display_t::always;
	if(s=="query" ) return vector_data_display_t::query ;
	if(s=="never" ) return vector_data_display_t::never ;
	return godefv::return_status_t{"unknown vector data display string \""+s+"\""};
}
void operator>>(ci::JsonTree const& config, vector_data_display_t& x){
	x=to_vector_data_display(config.getValue()).on_error(return_status_t::throw_{});
}

void operator>>(ci::JsonTree const& config, vector_data_t<auto>& x){
	std::unordered_map<std::string, float> size_map; load_if_exists(config, "size_map", size_map);
	shapefile_t shapefile(ci::app::getAssetPath(config.getValueForKey("path")).c_str());
	if constexpr(std::same_as<std::decay_t<decltype(x)>, vector_data_t<shapefile_point_t>>){
		x.data=shapefile.get_points(get_array<std::string>(config.getChild("name_keys")), config.getValueForKey("size_key"), size_map);
	}
	if constexpr(std::same_as<std::decay_t<decltype(x)>, vector_data_t<shapefile_line_t>>){
		x.data=shapefile.get_lines(get_array<std::string>(config.getChild("name_keys")), config.getValueForKey("size_key"), size_map);
	}
	if constexpr(std::same_as<std::decay_t<decltype(x)>, vector_data_t<shapefile_polygon_t>>){
		x.data=shapefile.get_polygons(get_array<std::string>(config.getChild("name_keys")));
	}
	load_if_exists(config, "color"         , x.color         );
	load_if_exists(config, "display"       , x.display       );
	load_if_exists(config, "min_zoom_level", x.min_zoom_level);
	load_if_exists(config, "min_size"      , x.min_size      );
	load_if_exists(config, "max_size"      , x.max_size      );
	load_if_exists(config, "size_offset"   , x.size_offset   );
	load_if_exists(config, "size_factor"   , x.size_factor   );
}

template<class ShapefileT>
void update_vector_data_indices(std::unordered_map<std::string, std::set<vector_database_index_t>>* indices, std::vector<vector_data_t<ShapefileT>> const& vector_data_list){
	for(auto source_index=0u; source_index<vector_data_list.size(); ++source_index){
		auto const& vector_data=vector_data_list[source_index];
		for(auto data_index=0u; data_index<vector_data.data.size(); ++data_index){
			auto const& shapefile=vector_data.data[data_index];
			auto index=vector_database_index_t{.type=get_vector_data_type<ShapefileT>(), .source_index=source_index, .data_index=data_index};
			//shapefile.name
				//|std::views::transform([](char c)->char{return std::tolower(c);})
				//|std::views::split(' ')
				//|std::views::transform([&](auto&& key){
					//(*indices)[std::string{key}]=index;
				//})
			//;
			for(auto key:split(to_lower(shapefile.name))){
				replace(key, ",", "");
				(*indices)[std::string{key}].insert(index);
			}
		}
	}
}

vector_database_t load_vector_database(ci::JsonTree const& config){
	vector_database_t result;
	load_if_exists(config, "point_data"  , result.point_data  );
	load_if_exists(config, "line_data"   , result.line_data   );
	load_if_exists(config, "polygon_data", result.polygon_data);
	update_vector_data_indices(&result.indices, result.point_data  );
	update_vector_data_indices(&result.indices, result.line_data   );
	update_vector_data_indices(&result.indices, result.polygon_data);
	return result;
}

ci::dvec2 get_position(std::variant<shapefile_point_t,shapefile_line_t,shapefile_polygon_t> const& shape_variant){
	return std::visit([&](auto const& shape){
		if constexpr(std::same_as<shapefile_point_t, std::decay_t<decltype(shape)>>){
			return shape.position;
		}else{
			return shape.parts[0].getPoints()[0];
		}
	}, shape_variant);
}

}
