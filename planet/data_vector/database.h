#pragma once

#include"type.h"
#include"shapefile.h"

#include"cinder/Color.h"
#include"cinder/Json.h"
#include"error_checking/return_status.h"

#include<string>
#include<variant>
#include<unordered_map>
#include<unordered_set>

namespace godefv::planet3d{

enum class vector_data_display_t{always,query,never,unknown};
auto to_vector_data_display(std::string const&)-> godefv::return_value_and_status_t<vector_data_display_t>;

template<class shapefileT>
struct vector_data_t{
	std::vector<shapefileT> data;
	ci::ColorAf color=ci::ColorAf::white();
	float min_zoom_level=5;

	vector_data_display_t display=vector_data_display_t::always;

	float size_factor=1;
	float size_offset=0;
	float min_size=1;
	float max_size=10;

	using shapefile_t=shapefileT;

	float size(float x) const{
		return std::clamp(size_offset+size_factor*x, min_size, max_size);
	}
};

struct vector_database_index_t{
	vector_data_type type;
	unsigned source_index;
	unsigned data_index;

	auto operator<=>(vector_database_index_t const&) const=default;
};

struct vector_database_t{
	std::unordered_map<std::string, std::set<vector_database_index_t>> indices;
	std::vector<vector_data_t<shapefile_point_t  >> point_data;
	std::vector<vector_data_t<shapefile_line_t   >> line_data;
	std::vector<vector_data_t<shapefile_polygon_t>> polygon_data;

	std::vector<vector_database_index_t> search(std::string const&) const;
	std::variant<shapefile_point_t,shapefile_line_t,shapefile_polygon_t> operator[](vector_database_index_t const&) const;
};

vector_database_t load_vector_database(ci::JsonTree const& config);

ci::dvec2 get_position(std::variant<shapefile_point_t,shapefile_line_t,shapefile_polygon_t> const& shape_variant);

}

