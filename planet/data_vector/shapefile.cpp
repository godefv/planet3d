#include"shapefile.h"

#include<iostream>
#include<numbers>

namespace godefv{

using std::numbers::pi;

shapefile_t::shapefile_t(std::filesystem::path const& path_)
	:path{path_}
{
	dataset=GDALDatasetUniquePtr{GDALDataset::Open(path.c_str(), GDAL_OF_VECTOR)};
	if(!dataset) throw std::runtime_error{std::string{"Unable to open shapefile file "}+path.c_str()};
}

std::string get_name(OGRFeature const& feature, std::vector<std::string> const& keys, std::string const& separator=", "){
	std::string result;
	for(auto const& key:keys){ 
		result+=feature[key.c_str()].GetAsString();
		result+=separator;
	}
	result=result.substr(0, result.size()-separator.size());
	return result;
}
float get_size(OGRFeature const& poFeature, std::string const& size_key, std::unordered_map<std::string, float> const& size_map){
	if(size_map.empty()){
		return static_cast<float>(poFeature[size_key.c_str()].GetAsDouble());
	}
	if(auto search=size_map.find(poFeature[size_key.c_str()].GetAsString()); search!=size_map.end()){
		return search->second;
	}
	return 1.f;
}

ci::dvec2 to_cinder(OGRPoint const& ogr_point){
	return ci::dvec2(ogr_point.getY(), ogr_point.getX());
}

std::vector<shapefile_point_t> shapefile_t::get_points(std::vector<std::string> const& name_keys, std::string const& size_key, std::unordered_map<std::string, float> const& size_map){
	std::vector<shapefile_point_t> result;
	for(OGRLayer* poLayer:dataset->GetLayers()){
		//std::cerr<<"Layer: "<<poLayer->GetName()<<std::endl;
		for(auto const& poFeature:*poLayer){
			const OGRGeometry *poGeometry = poFeature->GetGeometryRef();
			if(poGeometry == nullptr) continue;
			if(wkbFlatten(poGeometry->getGeometryType()) == wkbMultiPoint){
				std::cerr<<"Warning: wkbMultiPoint are not supported"<<std::endl;
			   	continue;
			}
			if(wkbFlatten(poGeometry->getGeometryType()) != wkbPoint) continue;
			result.push_back(shapefile_point_t{
				.name    =get_name(*poFeature, name_keys),
				.size    =get_size(*poFeature, size_key, size_map),
				.position=to_cinder(*poGeometry->toPoint())*(pi/180),
			});
		}
	}
	return result;
}

std::vector<ci::dvec2> get_vertices(OGRSimpleCurve const& line){
	std::vector<ci::dvec2> result;
	result.reserve(line.getNumPoints());
	for(auto i=0u; i<line.getNumPoints(); ++i){ 
		OGRPoint ogr_point; line.getPoint(i, &ogr_point);
		result.push_back(to_cinder(ogr_point)*(pi/180));
	}
	return result;
}

std::vector<shapefile_line_t> shapefile_t::get_lines(std::vector<std::string> const& name_keys, std::string const& size_key, std::unordered_map<std::string, float> const& size_map){
	std::vector<shapefile_line_t> result;
	for(OGRLayer* poLayer:dataset->GetLayers()){
		//std::cerr<<"Layer: "<<poLayer->GetName()<<std::endl;
		for(auto const& poFeature:*poLayer){
			const OGRGeometry *poGeometry = poFeature->GetGeometryRef();
			if(poGeometry == nullptr) continue;
			if(wkbFlatten(poGeometry->getGeometryType()) == wkbLineString){
				result.push_back(shapefile_line_t{
					.name =get_name(*poFeature, name_keys),
					.size =get_size(*poFeature, size_key, size_map),
					.parts={get_vertices(*poGeometry->toLineString())},
				});
				continue;
			}
			if(wkbFlatten(poGeometry->getGeometryType()) == wkbMultiLineString){
				auto const& ogr_lines=*poGeometry->toMultiLineString();
				shapefile_line_t line;
				line.name=get_name(*poFeature, name_keys);
				line.size=static_cast<float>((*poFeature)[size_key.c_str()].GetAsDouble());
				line.parts.reserve(ogr_lines.getNumGeometries());
				for(int part_id=0u; part_id<ogr_lines.getNumGeometries(); ++part_id){
					line.parts.push_back(get_vertices(*ogr_lines.getGeometryRef(part_id)));
				}
				result.push_back(line);
				continue;
			}
			std::cerr<<"Warning: unsupported line type."<<std::endl;
		}
	}
	return result;
}

std::vector<shapefile_polygon_t> shapefile_t::get_polygons(std::vector<std::string> const& name_keys){
	std::vector<shapefile_polygon_t> result;
	for(OGRLayer* poLayer:dataset->GetLayers()){
		//std::cerr<<"Layer: "<<poLayer->GetName()<<std::endl;
		for(auto const& poFeature:*poLayer){
			const OGRGeometry *poGeometry = poFeature->GetGeometryRef();
			if(poGeometry == nullptr) continue;
			if(wkbFlatten(poGeometry->getGeometryType()) == wkbPolygon){
				result.push_back(shapefile_polygon_t{
					.name =get_name(*poFeature, name_keys),
					.parts={get_vertices(*poGeometry->toPolygon()->getExteriorRing())},
				});
				continue;
			}
			if(wkbFlatten(poGeometry->getGeometryType()) == wkbMultiPolygon){
				auto const& ogr_polygons=*poGeometry->toMultiPolygon();
				shapefile_polygon_t polygon;
				polygon.name=get_name(*poFeature, name_keys);
				polygon.parts.reserve(ogr_polygons.getNumGeometries());
				for(int part_id=0u; part_id<ogr_polygons.getNumGeometries(); ++part_id){
					polygon.parts.push_back(get_vertices(*ogr_polygons.getGeometryRef(part_id)->getExteriorRing()));
				}
				result.push_back(polygon);
				continue;
			}
			std::cerr<<"Warning: unsupported polygon type."<<std::endl;
		}
	}
	return result;
}

}
