#include"routing.h"
#include"error_checking/return_status.h"
#include"error_checking/exit.h"

#include <osmscout/routing/RoutePostprocessor.h>
#include<osmscout/routing/RouteDescriptionPostprocessor.h>

int main(int argc, char** args){
	godefv::exit_if_false(argc==2, "Usage: test_routing <osmscout_database_directory>");
	auto router=godefv::planet3d::router_t{args[1]};

	auto const start =ci::dvec2{50.579601, 4.07125};
	auto const target=ci::dvec2{50.877096, 4.450946};
	auto route=router.get_fastest_route(start, target).on_error(godefv::return_status_t::exit{});

	auto const route_points=router.get_points(route);
	godefv::exit_if_true(route_points.empty(), "found an empty route");

	std::cout.precision(8);
	std::cout<<"From: "<<start<<std::endl;
	std::cout<<"To: "<<target<<std::endl;
	for (const auto& point:route_points) {
		std::cout<<point.x<<", "<<point.y<<std::endl;
	}
	return 0;
}

