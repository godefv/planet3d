#pragma once

#include"type.h"
#include"ogrsf_frmts.h"
#include"cinder/gl/gl.h"

#include<filesystem>

namespace godefv{

struct shapefile_point_t{
	std::string name;
	float size;
	ci::dvec2 position;
};

struct shapefile_line_t{
	std::string name;
	float size;
	std::vector<ci::PolyLine2d> parts;
};

struct shapefile_polygon_t{
	std::string name;
	float size;
	std::vector<ci::PolyLine2d> parts;
};

template<class T> vector_data_type get_vector_data_type();
template<> inline vector_data_type get_vector_data_type<shapefile_point_t  >(){return vector_data_type::point  ;}
template<> inline vector_data_type get_vector_data_type<shapefile_line_t   >(){return vector_data_type::line   ;}
template<> inline vector_data_type get_vector_data_type<shapefile_polygon_t>(){return vector_data_type::polygon;}

struct shapefile_t{
	std::filesystem::path path;
	GDALDatasetUniquePtr dataset;

	shapefile_t(std::filesystem::path const& path_);

	std::vector<shapefile_point_t  > get_points  (std::vector<std::string> const& name_keys, std::string const& size_key, std::unordered_map<std::string, float> const& size_map={});
	std::vector<shapefile_line_t   > get_lines   (std::vector<std::string> const& name_keys, std::string const& size_key, std::unordered_map<std::string, float> const& size_map={});
	std::vector<shapefile_polygon_t> get_polygons(std::vector<std::string> const& name_keys);
};

}
