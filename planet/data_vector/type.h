#pragma once

namespace godefv{

enum class vector_data_type{
	point,
	line,
	polygon,
};

}

