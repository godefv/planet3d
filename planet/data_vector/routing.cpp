#include"routing.h"

#include <cinder/Vector.h>
#include <osmscout/Database.h>
#include <osmscout/routing/RoutePostprocessor.h>
#include <osmscout/routing/DBFileOffset.h>
#include <osmscout/routing/RouteDescriptionPostprocessor.h>

#include<fstream>
#include<sstream>
#include<iostream>
#include<map>
#include<string>
#include<numbers>

namespace godefv::planet3d{
using std::numbers::pi;

router_t::router_t(std::filesystem::path const& database_directory, osmscout::DatabaseParameter databaseParameter, osmscout::RouterParameter routerParameter, std::string const& filenamebase)
{
	database=std::make_shared<osmscout::Database>(databaseParameter);
	database->Open(database_directory);
	router=std::make_shared<osmscout::SimpleRoutingService>(
			database,
			routerParameter,
			filenamebase
	);
	router->Open();
}

void GetCarSpeedTable(std::map<std::string,double>& map){
  map["highway_motorway"]=110.0;
  map["highway_motorway_trunk"]=100.0;
  map["highway_motorway_primary"]=70.0;
  map["highway_motorway_link"]=60.0;
  map["highway_motorway_junction"]=60.0;
  map["highway_trunk"]=100.0;
  map["highway_trunk_link"]=60.0;
  map["highway_primary"]=70.0;
  map["highway_primary_link"]=60.0;
  map["highway_secondary"]=60.0;
  map["highway_secondary_link"]=50.0;
  map["highway_tertiary_link"]=55.0;
  map["highway_tertiary"]=55.0;
  map["highway_unclassified"]=50.0;
  map["highway_road"]=50.0;
  map["highway_residential"]=20.0;
  map["highway_roundabout"]=40.0;
  map["highway_living_street"]=10.0;
  map["highway_service"]=30.0;
}

godefv::return_value_and_status_t<osmscout::RoutingResult> router_t::get_fastest_route(osmscout::GeoCoord start_coord, osmscout::GeoCoord target_coord, osmscout::RoutingParameter parameter){
	if(!database->IsOpen()){
		return godefv::return_status_t{"Invalid database ("+database->GetPath()+")"};
	}
	if (!router->IsOpen()) {
		return godefv::return_status_t{"Invalid router"};
	}

	osmscout::FastestPathRoutingProfileRef routingProfile=std::make_shared<osmscout::FastestPathRoutingProfile>(database->GetTypeConfig());
	routingProfile->SetPenaltySameType(this->penaltySameType);
	routingProfile->SetPenaltyDifferentType(this->penaltyDifferentType);
	routingProfile->SetMaxPenalty(this->maxPenalty);

	osmscout::TypeConfigRef      typeConfig=database->GetTypeConfig();
	std::map<std::string,double> carSpeedTable;

	switch (this->vehicle) {
		case osmscout::vehicleFoot:
			routingProfile->ParametrizeForFoot(*typeConfig, 5.0);
			break;
		case osmscout::vehicleBicycle:
			routingProfile->ParametrizeForBicycle(*typeConfig, 20.0);
			break;
		case osmscout::vehicleCar:
			GetCarSpeedTable(carSpeedTable);
			routingProfile->ParametrizeForCar(*typeConfig, carSpeedTable, 160.0);
			break;
	}

	auto startResult=router->GetClosestRoutableNode(start_coord, *routingProfile, osmscout::Kilometers(1));
	if (!startResult.IsValid()) {
		return godefv::return_status_t{"Error while searching for routing node near start location "+start_coord.GetDisplayText()};
	}
	osmscout::RoutePosition start=startResult.GetRoutePosition();
	if (start.GetObjectFileRef().GetType()==osmscout::refNode) {
		return godefv::return_status_t{"Cannot find node for start location!"};
	}

	auto targetResult=router->GetClosestRoutableNode(target_coord, *routingProfile, osmscout::Kilometers(1));
	if (!targetResult.IsValid()) {
		return godefv::return_status_t{"Error while searching for routing node near target location "+target_coord.GetDisplayText()};
	}
	osmscout::RoutePosition target=targetResult.GetRoutePosition();
	if (target.GetObjectFileRef().GetType()==osmscout::refNode) {
		return godefv::return_status_t{"Cannot find node for target location!"};
	}

	;
	osmscout::RoutingResult result=router->CalculateRoute(*routingProfile, start, target, parameter);
	if (!result.Success()) {
		return godefv::return_status_t{"There was an error while calculating the route!" };
	}

	return result;
}

godefv::return_value_and_status_t<osmscout::RoutingResult> router_t::get_fastest_route(ci::dvec2 start, ci::dvec2 target, osmscout::RoutingParameter parameter){
	return this->get_fastest_route(osmscout::GeoCoord{start.x, start.y}, osmscout::GeoCoord{target.x, target.y}, parameter);
}

std::vector<ci::dvec2> router_t::get_points(osmscout::RoutingResult const& route){
    osmscout::RoutePointsResult routePointsResult=this->router->TransformRouteDataToPoints(route.GetRoute());
    if(!routePointsResult.Success()) return {};

	std::vector<ci::dvec2> result; result.reserve(routePointsResult.GetPoints()->points.size());
	for(const auto& point : routePointsResult.GetPoints()->points){
		result.emplace_back(point.GetLat()*(pi/180), point.GetLon()*(pi/180));
	}
	return result;
}

}
