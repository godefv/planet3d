#include"database.h"
#include"error_checking/exit.h"

int main(int argc, char** argv){
	auto config=[](){
		try{
			return ci::JsonTree{ci::loadFile("config.json")};
		}catch(std::exception const& e ) {
			godefv::exit(std::string{"error loading config.json: "}+e.what());
		}
	}();
	GDALAllRegister();
	auto vector_database=godefv::planet3d::load_vector_database(config);
	for(auto index:vector_database.search(argc>1?argv[1]:"paz bolivia")){
		auto const result_variant=vector_database[index];
		std::visit([](auto const& result){
			std::cout<<result.name<<std::endl;
		}, result_variant);
	}

	return 0;
}

