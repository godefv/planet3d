#pragma once

#include<osmscout/routing/SimpleRoutingService.h>

#include"cinder/gl/gl.h"
#include"error_checking/return_status.h"

#include<string>
#include<chrono>
#include<filesystem>

namespace godefv::planet3d{

struct router_t{
	osmscout::DatabaseRef  database;
	osmscout::SimpleRoutingServiceRef router;

	osmscout::Vehicle      vehicle=osmscout::Vehicle::vehicleCar;
	osmscout::Distance     penaltySameType=osmscout::Meters(40);
	osmscout::Distance     penaltyDifferentType=osmscout::Meters(250);
	osmscout::HourDuration maxPenalty=std::chrono::seconds(10);

	router_t()=default;
	router_t(std::filesystem::path const& database_directory, osmscout::DatabaseParameter databaseParameter={}, osmscout::RouterParameter routerParameter={}, std::string const& filenamebase=osmscout::RoutingService::DEFAULT_FILENAME_BASE);

	godefv::return_value_and_status_t<osmscout::RoutingResult> get_fastest_route(osmscout::GeoCoord start, osmscout::GeoCoord target, osmscout::RoutingParameter parameter={});
	godefv::return_value_and_status_t<osmscout::RoutingResult> get_fastest_route(ci::dvec2 start, ci::dvec2 target, osmscout::RoutingParameter parameter={});

	std::vector<ci::dvec2> get_points(osmscout::RoutingResult const&);

};

}

