#include"shapefile.h"

int main(){
	GDALAllRegister();

	{
	godefv::shapefile_t shapefile("assets/gis_vector/naturalearthdata.com/ne_10m_populated_places.shp");
	auto const points=shapefile.get_points({"NAME", "ADM1NAME", "ADM0NAME"}, "NATSCALE");
	for(auto const& point:points){
		std::cout<<point.name<<": "<<point.position<<std::endl;
	}
	}
	{
	godefv::shapefile_t shapefile("assets/gis_vector/naturalearthdata.com/ne_10m_rivers_lake_centerlines.shp");
	auto const lines=shapefile.get_lines({"name"}, "scalerank");
	for(auto const& line:lines){
		std::cout<<line.name<<": ";
		for(auto const& position:line.parts[0].getPoints()) std::cout<<position<<"->";
		std::cout<<std::endl;
	}
	}
	{
	godefv::shapefile_t shapefile("assets/gis_vector/naturalearthdata.com/ne_10m_admin_0_countries.shp");
	auto const polygons=shapefile.get_polygons({"NAME", "CONTINENT"});
	for(auto const& polygon:polygons){
		std::cout<<polygon.name<<": ";
		for(auto const& position:polygon.parts[0].getPoints()) std::cout<<position<<"->";
		std::cout<<std::endl;
	}
	}

	return 0;
}

