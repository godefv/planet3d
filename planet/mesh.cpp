#include"mesh.h"
#include"projection/mercator.h"
#include"../utils/angle.h"

#include"error_checking/exit.h"

#include<cmath>
#include<numbers>
#include<limits>
#include<type_traits>
#include<vector>

using std::numbers::pi;

ci::gl::VboMeshRef make_planet_mesh(ci::ivec2 subdivisions, ci::vec2 latitude_range, ci::vec2 longitude_range){
	auto increment=ci::vec2(pi/subdivisions.x, 2*pi/subdivisions.y);
	
	auto projection=projection::mercator_t{};
	auto const min_latitude=projection.min_latitude();
	auto const max_latitude=projection.max_latitude();
	auto const make_latitude_increment=[&](auto latitude, int x){
		auto next_latitude=projection.normalized_to_angular(static_cast<float>(x+1)/subdivisions.x);
		return next_latitude-latitude;
	};

	// fill vertex data
	std::vector<ci::vec4> positions;
	std::vector<GLushort> indices;
	auto x= -subdivisions.x/2;
	for(auto latitude=min_latitude; latitude<max_latitude; latitude+=increment.x){
		increment.x=make_latitude_increment(latitude, x++);
		if(!(latitude_range.x<latitude && latitude<latitude_range.y)) continue;
		//change longitude range according to current latitude
		auto longitude_mean=(longitude_range.x+longitude_range.y)/2;
		auto adjusted_longitude_half_range=(longitude_range.y-longitude_range.x)/2/std::cos(latitude);
		auto current_longitude_range=ci::vec2(longitude_mean-adjusted_longitude_half_range, longitude_mean+adjusted_longitude_half_range);
		//
		for(auto longitude= -pi  ; longitude<pi  ; longitude+=increment.y){
			if(!is_angle_in_range(longitude, current_longitude_range.x, current_longitude_range.y)) continue;
		
			auto coords=ci::vec2(latitude,longitude);
			auto i=positions.size();
			positions.emplace_back(coords.x            , coords.y            ,  increment.x/2,  increment.y/2);
			positions.emplace_back(coords.x            , coords.y+increment.y,  increment.x/2, -increment.y/2);
			positions.emplace_back(coords.x+increment.x, coords.y            , -increment.x/2,  increment.y/2);
			positions.emplace_back(coords.x+increment.x, coords.y+increment.y, -increment.x/2, -increment.y/2);

			indices.push_back(i+0);
			indices.push_back(i+1);
			indices.push_back(i+2);

			indices.push_back(i+3);
			indices.push_back(i+2);
			indices.push_back(i+1);
	}}
	// fill poles
	auto fill_pole=[&](auto const latitude, auto const pole_latitude){
		auto pole_id=positions.size();
		positions.emplace_back(pole_latitude, 0, 0, 0);
		for(auto longitude= -pi  ; longitude<pi  ; longitude+=increment.y){
			if(!is_angle_in_range(longitude, longitude_range.x, longitude_range.y)) continue;
			auto coords=ci::vec2(latitude,longitude);
			auto i=positions.size();
			positions.emplace_back(coords.x            , coords.y            ,  -increment.x/2,  increment.y/2);
			positions.emplace_back(coords.x            , coords.y+increment.y,  -increment.x/2, -increment.y/2);
			indices.push_back(i+0);
			indices.push_back(i+1);
			indices.push_back(pole_id);
		}
	};
	fill_pole(min_latitude, -pi/2);
	fill_pole(max_latitude,  pi/2);

	std::cerr<<"Generated "<<positions.size()<<" planet vertices in "<<latitude_range<<"x"<<longitude_range<<"."<<std::endl;
	//std::cerr<<"Reserved  "<<square_count*4<<"."<<std::endl;
	godefv::exit_if_false(positions.size()<=std::numeric_limits<std::decay_t<decltype(indices[0])>>::max(), std::string{"Error generating planet mesh: "}+"too many vertices ("+std::to_string(positions.size())+") for vertex index type.");

	auto result=ci::gl::VboMesh::create(
		 positions.size(), GL_TRIANGLES
		,{ci::gl::VboMesh::Layout().usage(GL_STATIC_DRAW).attrib(ci::geom::AttribInfo{ci::geom::Attrib::POSITION, ci::geom::DataType::FLOAT,4,sizeof(positions[0]),0})}
		,indices.size(), GL_UNSIGNED_SHORT
	);

	result->bufferAttrib(ci::geom::Attrib::POSITION, positions);
	result->bufferIndices(indices.size()*sizeof(indices[0]), indices.data());

	return result;
}

