#pragma once

#include "cinder/gl/VboMesh.h"
#include "cinder/Vector.h"

ci::gl::VboMeshRef make_planet_mesh(ci::ivec2 subdivisions
	,ci::vec2 latitude_range =ci::vec2(-std::numbers::pi/2, std::numbers::pi/2)
	,ci::vec2 longitude_range=ci::vec2(-std::numbers::pi  , std::numbers::pi  )
);
