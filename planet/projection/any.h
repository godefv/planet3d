#pragma once

#include"mercator.h"
#include"even.h"

#include<variant>

namespace projection{
using any_t=std::variant<
	 mercator_t
	,even_t
>;

inline auto min_latitude(any_t const& any_projection){
	return std::visit([](auto const& projection){
		return projection.min_latitude();
	}, any_projection);
}
inline auto max_latitude(any_t const& any_projection){
	return std::visit([](auto const& projection){
		return projection.max_latitude();
	}, any_projection);
}

inline auto angular_to_normalized(any_t const& any_projection, float latitude){
	return std::visit([&](auto const& projection){
		return projection.angular_to_normalized(latitude);
	}, any_projection);
}
inline auto normalized_to_angular(any_t const& any_projection, float normalized_latitude){
	return std::visit([&](auto const& projection){
		return projection.normalized_to_angular(normalized_latitude);
	}, any_projection);
}

}

