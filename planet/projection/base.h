#ifndef PROJECTIONS_BASE_H
#define PROJECTIONS_BASE_H 

namespace projection{
template<class Derived>
struct base_t{
	Derived const* derived() const{return static_cast<Derived const*>(this);}
	float min_latitude() const{return derived()->normalized_to_angular(-0.5f);}
	float max_latitude() const{return derived()->normalized_to_angular( 0.5f);}
};
}

#endif /* PROJECTIONS_BASE_H */
