#ifndef PROJECTIONS_MERCATOR_H
#define PROJECTIONS_MERCATOR_H 

#include"base.h"

#include<cmath>
#include<numbers>

namespace projection{
struct mercator_t:public base_t<mercator_t>{
	//! Maps from [-0.5,0.5] to [min_latitude, max_latitude]
	float normalized_to_angular(float normalized_latitude) const{
		return 2*std::atan(std::exp(2*normalized_latitude*std::numbers::pi))-std::numbers::pi/2;
	}
	//! Maps from [min_latitude, max_latitude] to [-0.5,0.5]
	float angular_to_normalized(float latitude) const{
		return std::log(std::tan(std::numbers::pi/4+latitude/2))/(2*std::numbers::pi);
	}
};
}

#endif /* PROJECTIONS_MERCATOR_H */
