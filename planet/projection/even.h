#ifndef EVEN_H
#define EVEN_H 

#include"base.h"

#include<numbers>

namespace projection{
struct even_t:public base_t<even_t>{
	float normalized_to_angular(float normalized_latitude) const{
		return normalized_latitude*std::numbers::pi;
	}
	float angular_to_normalized(float latitude) const{
		return latitude/std::numbers::pi;
	}
};
}

#endif /* EVEN_H */
