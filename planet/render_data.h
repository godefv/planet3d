#pragma once

#include"data_raster/cache.h"
#include"projection/any.h"

#include"cinder/gl/gl.h"

#include<filesystem>

ci::gl::Texture3dRef load_texture_level(std::filesystem::path const& texture_directory, int level);

struct planet_render_data_t{
	enum class texture_type_t{
		color,
		elevation,
	};

	//parameters
	float radius=6371.f;
	ci::uvec2 subdivision_count;
	godefv::planet3d::texture_level_t texture_level={-1};

	//data
	ci::gl::BatchRef shape;
	ci::gl::GlslProgRef shader;
	godefv::planet3d::texture_cache_t textures;
	ci::gl::Texture2dRef texture_cache_lookup;
	godefv::planet3d::texture_cache_t elevation_map;
	ci::gl::Texture2dRef elevation_map_lookup;

	planet_render_data_t();

	void make_mesh(ci::uvec2 subdivision_count_
		,ci::vec2 latitude_range =ci::vec2(-std::numbers::pi/2, std::numbers::pi/2)
		,ci::vec2 longitude_range=ci::vec2(-std::numbers::pi  , std::numbers::pi  )
	);
	void load_texture_level(godefv::planet3d::texture_level_t level
		,ci::vec2 latitude_range 
		,ci::vec2 longitude_range
		,texture_type_t type
	);
	void load_texture_level(godefv::planet3d::texture_level_t level
		,ci::vec2 latitude_range 
		,ci::vec2 longitude_range
	);
};

