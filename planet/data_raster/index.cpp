#include"index.h"

namespace godefv::planet3d{

std::ostream& operator<<(std::ostream& out, texture_index_t const& x){
	//out<<"texture_index_t{level="<<x.level.value<<", latitude_id="<<x.latitude_id<<", longitude_id="<<x.longitude_id<<"}";
	out<<"{level:"<<x.level.value<<", position: "
		<<"("<<std::setw(2)<<x.latitude_id
		<<","<<std::setw(2)<<x.longitude_id
		<<")}";
	return out;
}

}
