#include"geotiff.h"

#include<iostream>
 
int main() { 
	GDALAllRegister();

	{
	// create object of Geotiff class
	Geotiff tiff("assets/gis_raster/elevation_map_aster/ASTGTMV003_N27E003_dem.tif");  

	// output a value from 2D array  
	auto rasterBandData = tiff.GetRasterBand(1) ; 
	std::cout<<"value at row 0, column 0: "<<rasterBandData.getValue(ci::ivec2(0,0))<<std::endl; 
	std::cout<<"value at row 1000, column 1000: "<<rasterBandData.getValue(ci::ivec2(1000,1000))<<std::endl; 

	// call other methods, like get the name of the Geotiff
	// passed-in, its length, and its projection string 
	std::cout<<tiff.GetProjection()<<std::endl;

	// dump out the Geotransform (6 element array of doubles) 
	auto gt = tiff.GetGeoTransform(); 
	std::cout<<gt[0]<<" "<<gt[1]<<" "<<gt[2]<<" "<<gt[3]<<" "<<gt[4]<<" "<<gt[5]<<std::endl; 

	// dump out Geotiff band NoData value (often it is -9999.0)
	std::cout<<"No data value: "<<tiff.GetNoDataValue()<<std::endl;  

	// dump out array (band) dimensions of Geotiff data  
	std::cout<<tiff.NLEVELS<<" "<<tiff.NROWS<<" "<<tiff.NCOLS<<std::endl;
	}

	GDALDestroyDriverManager();
	return 0;
}
