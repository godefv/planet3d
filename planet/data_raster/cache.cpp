#include"cache.h"
#include"../../utils/cinder/image.h"

#include"cinder/app/App.h"

#include<algorithm>

namespace godefv::planet3d{
namespace gl=ci::gl;

auto getInternalDataFormat(ci::Surface8u  const& image){return image.hasAlpha()?GL_RGBA:GL_RGB;}
auto getInternalDataFormat(ci::Channel16u const& image){return GL_R16UI;}

auto getDataFormat(ci::Surface8u  const& image){return image.hasAlpha()?GL_RGBA:GL_RGB;}
auto getDataFormat(ci::Channel16u const& image){return GL_RED_INTEGER;}

auto getDataType(ci::Surface8u  const& image){return GL_UNSIGNED_BYTE;}
auto getDataType(ci::Channel16u const& image){return GL_UNSIGNED_SHORT;}

auto getFilter(ci::Surface8u  const& image){return GL_LINEAR;}
auto getFilter(ci::Channel16u const& image){return GL_NEAREST;}

texture_index_t normalize(texture_source::any_t const& texture_source, texture_index_t const& texture_index){
	auto const texture_count=ci::ivec2(texture_source.get_texture_count(texture_index.level));
	auto const positive_longitude_id=(texture_index.longitude_id+texture_count.y)%texture_count.y;
	return texture_index_t{
		.level       =texture_index.level,
		.latitude_id =std::clamp(texture_index.latitude_id, 0, texture_count.x-1),
		.longitude_id=std::clamp(positive_longitude_id    , 0, texture_count.y-1),
	};
}

texture_cache_t::texture_cache_t(texture_source::any_t const& texture_source_, int size)
	:texture_source{texture_source_}
{
	GLint max_object_pool_size; glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &max_object_pool_size);
	object_pool_size=std::min(max_object_pool_size, size);

	std::visit([&](auto const& source){
		//create texture
		auto image=source.load_any_texture();
		auto format=gl::Texture3d::Format{}
			.target(GL_TEXTURE_2D_ARRAY)
			.mipmap(false)
			.wrap(GL_CLAMP_TO_EDGE)
			.minFilter(getFilter(image))
			.magFilter(getFilter(image))
		;
		format.setInternalFormat(getInternalDataFormat(image));
		object_pool=gl::Texture3d::create(image.getWidth(), image.getHeight(), object_pool_size, format);
		//load the default texture to use when a texture is missing
		clear(&image);
		object_pool->update(image.getData(), getDataFormat(image), getDataType(image), 0, object_pool->getWidth(), object_pool->getHeight(), 1, 0, 0, this->next_object_pool_position);
	}, texture_source.variant);
	object_pool_content.resize(object_pool_size);
}

void texture_cache_t::load(texture_index_t const& texture_index){
	try{
		//Load texture from disk to GPU
		std::visit([&](auto const& source){
			auto image=source.load_texture(texture_index);
			object_pool->update(image.getData(), getDataFormat(image), getDataType(image), 0, image.getWidth(), image.getHeight(), 1, 0, 0, this->next_object_pool_position);
		}, texture_source.variant);
		//Update index
		object_pool_content[this->next_object_pool_position]=texture_index;
		++this->next_object_pool_position;
		if(this->next_object_pool_position>=object_pool_content.size()){
			this->next_object_pool_position=1;
		}
		//Log
		std::cerr<<"Loaded texture "<<texture_index<<" to "<<std::setw(4)<<this->next_object_pool_position<<std::endl;
	}catch(std::exception const& e){
		std::cerr<<"error loading texture "<<e.what()<<std::endl;
	}
}

void texture_cache_t::load(texture_index_t begin, texture_index_t end){
	for(auto longitude_id=begin.longitude_id; longitude_id<end.longitude_id; ++longitude_id){
	for(auto latitude_id =begin.latitude_id ; latitude_id <end.latitude_id ; ++latitude_id ){
		auto texture_index=normalize(texture_source, texture_index_t{
			.level=begin.level,
			.latitude_id =latitude_id ,
			.longitude_id=longitude_id,
		});
		if(std::ranges::find(object_pool_content, texture_index)!=object_pool_content.end()) continue;
		this->load(texture_index);
	}}
}

}
