#include"geotiff.h"

Geotiff::Geotiff(std::filesystem::path const& path_) 
	:path{path_}
{ 
	// set pointer to Geotiff dataset as class member.  
	dataset = GDALDatasetUniquePtr{GDALDataset::Open(path.c_str(), GA_ReadOnly)};
	if(!dataset) throw std::runtime_error{std::string{"Unable to open Geotiff file "}+path.c_str()};

	// set the dimensions of the Geotiff 
	NROWS   = GDALGetRasterYSize( dataset.get() ); 
	NCOLS   = GDALGetRasterXSize( dataset.get() ); 
	NLEVELS = GDALGetRasterCount( dataset.get() );

}

ci::Channel16u Geotiff::GetRasterBand(int layer_index) {
	auto bandLayer=ci::Channel16u{NCOLS, NROWS};
	CPLErr e=dataset->GetRasterBand(layer_index)->RasterIO(
			GF_Read,0,0,NCOLS,NROWS
		,bandLayer.getData(),NCOLS,NROWS,GDT_Int16
		,0,0
	);
	if(e!=0) throw std::runtime_error{std::string{"Unable to read Geotiff file"}+path.c_str()};
	return bandLayer;
}

