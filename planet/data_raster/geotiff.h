#pragma once

#include"cinder/Channel.h"

#include"gdal_priv.h"
#include"cpl_conv.h"
#include"gdalwarper.h"

#include<filesystem>
#include<string>

struct Geotiff { 
	std::filesystem::path path;
	GDALDatasetUniquePtr dataset;
	int NROWS,NCOLS,NLEVELS;    

	Geotiff(std::filesystem::path const& path); 

	std::string GetProjection() { 
		return dataset->GetProjectionRef(); 
	} 

	std::array<double,6> GetGeoTransform() {
		std::array<double,6> geotransform;
		dataset->GetGeoTransform(geotransform.data());
		return geotransform; 
	} 

	double GetNoDataValue() { 
		return static_cast<double>(dataset->GetRasterBand(1)->GetNoDataValue());
	}

	ci::Channel16u GetRasterBand(int layer_index);
};

inline ci::Channel16u load_geotiff(std::filesystem::path const& path){
	return Geotiff{path}.GetRasterBand(1);
}

