#pragma once

#include"index.h"
#include"sources/any.h"

#include"cinder/gl/gl.h"

#include<filesystem>
#include<iostream>

namespace godefv::planet3d{
	
struct texture_cache_t{
	//parameters
	texture_source::any_t texture_source;
	int object_pool_size=2048;
	//data
	//[object_pool_content.size(), object_pool_size[ is empty
	//when full, object_pool_content becomes a circular buffer
	ci::gl::Texture3dRef object_pool;
	std::vector<texture_index_t> object_pool_content;
	int next_object_pool_position=1;

	texture_cache_t()=default;
	texture_cache_t(texture_source::any_t const& texture_source_, int size=2048);

	void load(texture_index_t const& texture_index);
	void load(texture_index_t begin, texture_index_t end);
};

}

