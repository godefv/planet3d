#pragma once

#include<compare>
#include<iostream>
#include<iomanip>

namespace godefv::planet3d{

struct texture_level_t{
	int value=-1;
	auto operator<=>(texture_level_t const&) const=default;
};

struct texture_index_t{
	texture_level_t level;
	int latitude_id;
	int longitude_id;

	auto operator<=>(texture_index_t const&) const=default;
};

std::ostream& operator<<(std::ostream& out, texture_index_t const& x);

}
