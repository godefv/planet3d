#pragma once

#include"../index.h"
#include"../../projection/even.h"

#include"cinder/gl/gl.h"

#include<filesystem>

namespace godefv::planet3d::texture_source{

struct aster_t{
	std::filesystem::path path;
	std::string url_format;
	static constexpr projection::even_t projection{};

	ci::uvec2 get_texture_count(texture_level_t level) const;

	std::filesystem::path get_texture_path_relative_to_texture_level_directory(int latitude_degrees, int longitude_degrees) const;

	std::filesystem::path get_texture_path(texture_index_t const& texture_index) const;
	std::string get_texture_url(texture_index_t const& texture_index) const;

	ci::Channel16u load_texture(texture_index_t const& texture_index) const;

	ci::Channel16u load_any_texture() const;
};

}

