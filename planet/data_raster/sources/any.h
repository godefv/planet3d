#pragma once

#include"gmapcatcher.h"
#include"aster.h"
#include"../../projection/any.h"

#include"error_checking/return_status.h"

#include<variant>


namespace godefv::planet3d::texture_source{
	
struct any_t{
	using variant_t=std::variant<
		gmapcatcher_t
		,aster_t
	>;
	variant_t variant;

	inline auto get_texture_count(texture_level_t const& texture_level) const{
		return std::visit([&](auto const& texture_source){
			return texture_source.get_texture_count(texture_level);
		}, this->variant);
	}

	inline auto get_texture_path(texture_index_t const& texture_index) const{
		return std::visit([&](auto const& texture_source){
			return texture_source.get_texture_path(texture_index);
		}, this->variant);
	}

	inline auto get_texture_url(texture_index_t const& texture_index) const{
		return std::visit([&](auto const& texture_source)->std::string{
			if(texture_source.url_format.empty()) return {};
			return texture_source.get_texture_url(texture_index);
		}, this->variant);
	}

	//inline auto load_texture(texture_index_t const& texture_index) const{
		//return std::visit([&](auto const& texture_source){
			//return texture_source.load_texture(texture_index);
		//}, this->variant);
	//}

	inline auto get_projection() const{
		return std::visit([](auto const& texture_source){
			return projection::any_t{texture_source.projection};
		}, this->variant);
	}
};

inline auto load(std::string const& type, std::filesystem::path const& path)->godefv::return_value_and_status_t<any_t>{
	if(type=="gmapcatcher"){
		return any_t{gmapcatcher_t{path}};
	}
	if(type=="aster"){
		return any_t{aster_t{path}};
	}
	return godefv::return_status_t{"unknown texture source type \""+type+"\""};
}

}
