#include "aster.h"
#include"../geotiff.h"

#include<iostream>
#include<iomanip>

namespace godefv::planet3d::texture_source{

inline std::string to_string(int value, int digit_count){
	std::ostringstream os;
	os<<std::setfill('0')<<std::setw(digit_count)<<value;
	return os.str();
}

ci::uvec2 aster_t::get_texture_count(texture_level_t level) const{
	return ci::uvec2(180,360);
}

std::filesystem::path aster_t::get_texture_path_relative_to_texture_level_directory(int latitude_degrees, int longitude_degrees) const{
	return std::string{"ASTGTMV003_"}+(latitude_degrees>=0?"N":"S")+to_string(std::abs(latitude_degrees), 2)+(longitude_degrees>=0?"E":"W")+to_string(std::abs(longitude_degrees), 3)+"_dem.tif";
}

std::filesystem::path aster_t::get_texture_path(texture_index_t const& texture_index) const{
	auto const texture_count=get_texture_count(texture_index.level);
	return path/get_texture_path_relative_to_texture_level_directory(texture_index.latitude_id-texture_count.x/2, texture_index.longitude_id-texture_count.y/2);
}

ci::Channel16u aster_t::load_texture(texture_index_t const& texture_index) const{
	return load_geotiff(get_texture_path(texture_index));
}

ci::Channel16u aster_t::load_any_texture() const{
	return load_geotiff(path/get_texture_path_relative_to_texture_level_directory(0,6));
}

}

