#include"gmapcatcher.h"

#include<iomanip>

namespace godefv::planet3d::texture_source{

ci::uvec2 gmapcatcher_t::get_texture_count(texture_level_t level) const{
	return ci::uvec2(std::pow(2, level.value+1));
}

std::filesystem::path gmapcatcher_t::get_texture_level_directory(texture_level_t level) const{
	return path/std::to_string(16-level.value);
}

std::filesystem::path gmapcatcher_t::get_texture_path_relative_to_texture_level_directory(int latitude_id, int longitude_id) const{
	return std::filesystem::path{std::to_string(longitude_id/1024)}/std::to_string(longitude_id%1024)/std::to_string(latitude_id/1024)/(std::to_string(latitude_id%1024)+".png");
}

std::filesystem::path gmapcatcher_t::get_texture_path(texture_index_t const& texture_index) const{
	auto const texture_count=get_texture_count(texture_index.level);
	return get_texture_level_directory(texture_index.level)/get_texture_path_relative_to_texture_level_directory(texture_count.x-1-texture_index.latitude_id, texture_index.longitude_id);
}

std::string make_0123_recursive_coordinates(texture_index_t const& texture_index){
	std::string result;
	for(auto level=0u; level<=texture_index.level.value; ++level){
		int digit=0;
		auto mask=1<<(texture_index.level.value-level);
        if(texture_index.longitude_id&mask) digit += 1;
        if(texture_index.latitude_id &mask) digit += 2;
		result.append(std::to_string(digit));
	}
	return result;
}

std::string gmapcatcher_t::get_texture_url(texture_index_t const& texture_index) const{
	ci::ivec2 const texture_count=get_texture_count(texture_index.level);
	auto const gmapcatcher_texture_index=texture_index_t{
		.level=texture_index.level,
		.latitude_id=texture_count.x-1-texture_index.latitude_id,
		.longitude_id=texture_index.longitude_id
	};
	auto result=url_format;
	replace(result, "{0123_recursive_coordinates}", make_0123_recursive_coordinates(gmapcatcher_texture_index));
	replace(result, "{latitude_id}" , std::to_string(gmapcatcher_texture_index.latitude_id));
	replace(result, "{longitude_id}", std::to_string(gmapcatcher_texture_index.longitude_id));
	replace(result, "{zoom_level}"  , std::to_string(gmapcatcher_texture_index.level.value+1));
	return result;
}

ci::Surface gmapcatcher_t::load_texture(texture_index_t const& texture_index) const{
	auto const texture_path=get_texture_path(texture_index);
	std::error_code error;
	if(std::filesystem::exists(texture_path, error)){
		return ci::loadImage(texture_path.c_str());
	}
	auto const texture_url=get_texture_url(texture_index);
	if(!texture_url.empty()){
		ci::Surface result=ci::loadImage(ci::loadUrl(texture_url));
		try{
			ci::writeImage(ci::writeFile(texture_path.c_str()), result);
			std::cerr<<"Cached texture "<<std::quoted(texture_path.c_str())<<" from "<<texture_url<<"."<<std::endl;
		}catch(std::exception const& e){
			std::cerr<<"Could not cache texture "<<std::quoted(texture_path.c_str())<<" from "<<texture_url<<": "<<e.what()<<std::endl;
		}
		return result;
	}
	throw std::runtime_error{std::string{}+"texture file not found at "+texture_path.c_str()+"and no url is set for downloading."};
}

ci::Surface gmapcatcher_t::load_any_texture() const{
	return load_texture(texture_index_t{{0}, 0,0});
}

}
