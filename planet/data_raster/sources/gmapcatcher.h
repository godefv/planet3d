#pragma once

#include"../index.h"
#include"../../projection/mercator.h"
#include"../../../utils/string.h"

#include"cinder/gl/gl.h"
#include"cinder/app/App.h"

#include<filesystem>
#include<cmath>

namespace godefv::planet3d::texture_source{

struct gmapcatcher_t{
	std::filesystem::path path;
	std::string url_format;
	static constexpr projection::mercator_t projection{};

	ci::uvec2 get_texture_count(texture_level_t level) const;
	std::filesystem::path get_texture_level_directory(texture_level_t level) const;
	std::filesystem::path get_texture_path_relative_to_texture_level_directory(int latitude_id, int longitude_id) const;
	std::filesystem::path get_texture_path(texture_index_t const& texture_index) const;
	std::string get_texture_url(texture_index_t const& texture_index) const;
	ci::Surface load_texture(texture_index_t const& texture_index) const;
	ci::Surface load_any_texture() const;
};

}
