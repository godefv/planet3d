#include"render_data.h"
#include"mesh.h"
#include"../utils/cinder/image.h"

#include"cinder/app/App.h"
#include"error_checking/exit.h"

#include<numbers>
#include<algorithm>
#include<iostream>
#include<iomanip>

using namespace godefv::planet3d;
namespace gl=ci::gl;
using std::numbers::pi;

planet_render_data_t::planet_render_data_t(){
	shader=gl::GlslProg::create(gl::GlslProg::Format{}
		.vertex  (ci::app::loadAsset("shaders/planet.vert")) 
		.fragment(ci::app::loadAsset("shaders/planet.frag"))
	);
	texture_cache_lookup=gl::Texture2d::create(256, 256, gl::Texture2d::Format()
		.mipmap(false)
		.internalFormat(GL_R16UI)
		.minFilter(GL_NEAREST)
		.magFilter(GL_NEAREST)
	);
	elevation_map_lookup=gl::Texture2d::create(256, 256, gl::Texture2d::Format()
		.mipmap(false)
		.internalFormat(GL_R16UI)
		.minFilter(GL_NEAREST)
		.magFilter(GL_NEAREST)
	);
}

void planet_render_data_t::make_mesh(ci::uvec2 subdivision_count_, ci::vec2 latitude_range, ci::vec2 longitude_range){
	subdivision_count=subdivision_count_;
	shader->uniform("radius", radius);
	shader->uniform("subdivision_count", subdivision_count);
	shape=gl::Batch::create(make_planet_mesh(subdivision_count, latitude_range, longitude_range), shader);
}

ci::Channel16u make_lookup_texture(ci::uvec2 size, ci::ivec2 offset, texture_cache_t const& texture_cache, texture_level_t level){
	auto const texture_count=texture_cache.texture_source.get_texture_count(level);
	auto lookup_texture=ci::Channel16u(size.x, size.y);
	clear(&lookup_texture);
	for(auto i=0u; i<texture_cache.object_pool_content.size(); ++i){
		auto const& texture_index=texture_cache.object_pool_content[i];
		if(texture_index.level!=level) continue;
		auto const local_texture_id=(ci::ivec2(texture_index.latitude_id, texture_index.longitude_id)-offset+ci::ivec2(texture_count))%ci::ivec2(texture_count);
		lookup_texture.setValue(ci::ivec2(local_texture_id.x, size.y-1-local_texture_id.y), i);
	}
	return lookup_texture;
}

void load_texture_level(texture_level_t level, ci::vec2 latitude_range, ci::vec2 longitude_range, texture_cache_t* texture_cache, gl::Texture2dRef lookup_texture, std::string const& uniform_prefix, ci::gl::GlslProgRef shader){
	std::cerr<<"Loading "<<uniform_prefix<<" texture level "<<level.value<<" in "<<latitude_range<<"x"<<longitude_range<<"."<<std::endl;
	auto const& projection=texture_cache->texture_source.get_projection();
	latitude_range.x=std::clamp(latitude_range.x, min_latitude(projection), max_latitude(projection));
	latitude_range.y=std::clamp(latitude_range.y, min_latitude(projection), max_latitude(projection));

	auto const texture_count=texture_cache->texture_source.get_texture_count(level);

	auto texture_id_offset=ci::ivec2(texture_count);
	auto texture_max_ids=ci::ivec2(0);
	for(auto normalized_latitude=angular_to_normalized(projection, latitude_range.x); normalized_latitude<=angular_to_normalized(projection, latitude_range.y); normalized_latitude+=1.0/texture_count.x){
		auto latitude=normalized_to_angular(projection, normalized_latitude);
		//change longitude range according to current latitude
		auto longitude_mean=(longitude_range.x+longitude_range.y)/2;
		auto adjusted_longitude_half_range=(longitude_range.y-longitude_range.x)/2/std::cos(latitude);
		auto current_longitude_range=ci::vec2(longitude_mean-adjusted_longitude_half_range, longitude_mean+adjusted_longitude_half_range);
		//
		auto const make_texture_coordinates=[&](float normalized_latitude, float longitude){
			return (ci::vec2(normalized_latitude, longitude/(2*pi))+ci::vec2(0.5))*ci::vec2(texture_count);
		};
		auto min_texture_ids=ci::ivec2(floor(make_texture_coordinates(normalized_latitude, current_longitude_range.x)));
		auto max_texture_ids=ci::ivec2(ceil (make_texture_coordinates(normalized_latitude+1.0/texture_count.x, current_longitude_range.y)));
		std::cerr<<"Loading "<<uniform_prefix<<" textures from "<<min_texture_ids<<" to "<<max_texture_ids<<"."<<std::endl;
		texture_id_offset=min(texture_id_offset, min_texture_ids);
		texture_max_ids=min(texture_max_ids, max_texture_ids);
		//
		texture_cache->load(texture_index_t{
				.level=level,
				.latitude_id =min_texture_ids.x,
				.longitude_id=min_texture_ids.y,
			}
			,texture_index_t{
				.level=level,
				.latitude_id =max_texture_ids.x,
				.longitude_id=max_texture_ids.y,
			}
		);
	}
	godefv::exit_if_false(
		(texture_max_ids-texture_id_offset).x<lookup_texture->getSize().x &&
		(texture_max_ids-texture_id_offset).y<lookup_texture->getSize().y 
		, (std::stringstream{}<<"lookup texture size is too small: "<<lookup_texture->getSize()<<"<="<<(texture_max_ids-texture_id_offset)).str()
	);

	shader->uniform(uniform_prefix+"_texture_id_offset", texture_id_offset);
	shader->uniform(uniform_prefix+"_texture_count"    , texture_count);
	lookup_texture->update(make_lookup_texture(lookup_texture->getSize(), texture_id_offset, *texture_cache, level));
}

void planet_render_data_t::load_texture_level(texture_level_t level, ci::vec2 latitude_range, ci::vec2 longitude_range, planet_render_data_t::texture_type_t type){
	auto [texture_cache, lookup_texture, uniform_prefix]=[&]()->std::tuple<texture_cache_t*, gl::Texture2dRef, std::string>{
		switch(type){
		case texture_type_t::color:     return std::make_tuple(&this->textures     , this->texture_cache_lookup, "color"    );
		case texture_type_t::elevation: return std::make_tuple(&this->elevation_map, this->elevation_map_lookup, "elevation");
		}
		throw std::runtime_error{"Unexpected planet_render_data_t::texture_type_t: "+std::to_string(static_cast<int>(type))};
	}();
	::load_texture_level(level, latitude_range, longitude_range, texture_cache, lookup_texture, uniform_prefix, this->shader);
	this->texture_level=level;
}

void planet_render_data_t::load_texture_level(texture_level_t level, ci::vec2 latitude_range, ci::vec2 longitude_range){
	this->load_texture_level(level, latitude_range, longitude_range, texture_type_t::color);
	if(level.value>=8){
	this->load_texture_level({8}, latitude_range, longitude_range, texture_type_t::elevation);
	}
}
