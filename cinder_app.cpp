#include"cinder/gl/gl.h"
#include"cinder_app.h"
#include"user_interface.h"
#include"utils/angle.h"
#include"utils/math.h"
#include"utils/gl_debug_output.h"

#include <cinder/Color.h>
#include<cinder/gl/wrapper.h>
#include<numbers>
#include<algorithm>

namespace godefv::planet3d{

namespace gl=ci::gl;
using std::numbers::pi;

void cinder_app::mouseDown(ci::app::MouseEvent event){
	if(event.isLeft()){
		last_mouse_position=event.getPos();
	}
}

void cinder_app::mouseDrag(ci::app::MouseEvent event){
	if(!event.isLeftDown()) return;
	auto dX=event.getPos()-std::exchange(last_mouse_position, event.getPos());
	spectator_position.x+=spectator_position.z*drag_speed*dX.y;
	spectator_position.y-=spectator_position.z*drag_speed*dX.x;

	spectator_position.x=std::clamp(spectator_position.x, -pi/2, pi/2);
	spectator_position.y=project_angle(spectator_position.y, -pi, pi);

	spectator_position_has_changed=true;
}

void cinder_app::mouseWheel(ci::app::MouseEvent event){
	spectator_position.z*=std::pow(1.2, -event.getWheelIncrement());

	spectator_position_has_changed=true;
}

void cinder_app::keyDown(ci::app::KeyEvent key){
	switch(key.getChar()){
		case 'r':
		this->dispatchAsync([&](){
			this->assets=load_assets();
			spectator_position_has_changed=true;
		});
		return;
	}
}

void cinder_app::resize(){
	ci::ivec2 size=this->getWindow()->getSize();
	std::cerr<<"New window size: "<<size<<std::endl;
	spectator_camera.setFov(180/pi*std::atan(static_cast<float>(size.y)/this->reference_window_size.y*std::tan(this->reference_fov*pi/180)));
	spectator_camera.setAspectRatio(static_cast<float>(size.x)/size.y);
	this->user_interface.graph->setNeedsLayout();
}

void cinder_app::setup(){
	enable_gl_debug_output();
	this->getWindow()->setSize(800, 600);
	GDALAllRegister();
	this->assets=load_assets();

	//spectator
	this->spectator_camera.setWorldUp(ci::vec3(0, 0, 1));
	this->spectator_camera.setFarClip(200000.f);

	//user interface
	this->user_interface.setup(assets, &spectator_position);
}

void cinder_app::update(){
	//set camera for non vr view
	auto const cos_latitude=std::cos(spectator_position.x);
	auto const spectator_position_cubic=(this->assets.planet.radius+this->spectator_position.z)*ci::dvec3(cos_latitude*std::cos(spectator_position.y), cos_latitude*std::sin(spectator_position.y), std::sin(spectator_position.x));
	this->spectator_camera.lookAt(spectator_position_cubic, ci::vec3(0, 0, 0));
	this->assets.planet.shader->uniform("view_coordinates", ci::vec3(spectator_position.x, spectator_position.y, spectator_position.z));

	//update texture level
	this->zoom_level=std::max(0, static_cast<int>(-std::log(this->spectator_position.z/this->altitude_for_texture_level_0)/std::log(2)));
	if(spectator_position_has_changed){
		std::cerr<<"Zoom level "<<zoom_level<<std::endl;
		//compute subdivision count 
		auto subdivision_count=ci::uvec2(32,32);
		if(zoom_level>3) subdivision_count*=std::pow(2, zoom_level-3);
		//compute view range
		this->view_size=ci::vec2(pi, 2*pi);
		if(zoom_level>2){
			view_size*=this->spectator_position.z/(this->altitude_for_texture_level_0/std::pow(2,2))*1.2;
			if(zoom_level<=4) view_size*=2;
			std::cerr<<view_size<<std::endl;
		}
		//update planet data
		this->assets.planet.make_mesh(subdivision_count
			,ci::vec2(spectator_position.x-view_size.x/2, spectator_position.x+view_size.x/2)
			,ci::vec2(spectator_position.y-view_size.y/2, spectator_position.y+view_size.y/2)
		);
		this->assets.planet.load_texture_level({zoom_level}
			,ci::vec2(spectator_position.x-view_size.x/2, spectator_position.x+view_size.x/2)
			,ci::vec2(spectator_position.y-view_size.y/2, spectator_position.y+view_size.y/2)
		);
		spectator_position_has_changed=false;
	}

	this->user_interface.graph->propagateUpdate();
}

void cinder_app::draw(){
	gl::enableDepthWrite();
	gl::enableDepthRead();
	gl::clear();

	gl::pushMatrices();
	gl::setMatrices(this->spectator_camera);

	auto const latitude_range =ci::vec2(spectator_position.x-view_size.x/2, spectator_position.x +view_size.x/2);
	auto const longitude_range=ci::vec2(spectator_position.y-view_size.y/2, spectator_position.y+view_size.y/2);

	//stars
	if(this->assets.stars_texture){
		gl::pushMatrices();
		gl::translate(this->spectator_camera.getEyePoint());
		gl::ScopedTextureBind texture(this->assets.stars_texture);
		gl::disable(GL_CULL_FACE);
		gl::depthMask(GL_FALSE);
		this->assets.stars->draw();
		gl::depthMask(GL_TRUE);
		gl::popMatrices();
	}
	{//planet
	if(this->assets.planet.textures.object_pool){
		this->assets.planet.textures.object_pool->bind(0);
		this->assets.planet.texture_cache_lookup->bind(1);
		this->assets.planet.shader->uniform("color_texture_array", 0);
		this->assets.planet.shader->uniform("color_texture_array_lookup", 1);
	}
	if(this->assets.planet.elevation_map.object_pool){
		this->assets.planet.elevation_map.object_pool->bind(2);
		this->assets.planet.elevation_map_lookup->bind(3);
		this->assets.planet.shader->uniform("elevation_texture_array", 2);
		this->assets.planet.shader->uniform("elevation_texture_array_lookup", 3);
	}
	this->assets.planet.shape->draw();
	}
	gl::disable(GL_DEPTH_TEST);
	{//vector data
	assets.vector_data_shader->bind();
	auto const draw_vector_data=[&](vector_data_t<auto> const& vector_data){
		if(vector_data.display==vector_data_display_t::never) return;
		if(this->zoom_level<vector_data.min_zoom_level) return;
		assets.vector_data_shader->uniform("color", vector_data.color);
		for(auto const& shape:vector_data.data){
			if constexpr(std::same_as<shapefile_point_t, std::decay_t<decltype(shape)>>){
				ci::vec2 position=shape.position;
				if(!is_angle_in_range(position.x, latitude_range.x, latitude_range.y)) continue;
				if(!is_angle_in_range(position.y, longitude_range.x, longitude_range.y)) continue;
				gl::drawCube(ci::vec3(shape.position, this->assets.planet.radius+2), ci::vec3(vector_data.size(shape.size)*0.001f*ci::vec2(1.f, 1.f/cos(shape.position.x)), 2));
			}else{
				gl::lineWidth(vector_data.size(shape.size));
				for(auto const& part:shape.parts){
					ci::vec2 position=part.getPoints()[0];
					if(!is_angle_in_range(position.x, latitude_range.x, latitude_range.y)) continue;
					if(!is_angle_in_range(position.y, longitude_range.x, longitude_range.y)) continue;
					gl::draw(part);
				}
			}
		}
	};
	for(auto const& points  :assets.vector_database.point_data  ) draw_vector_data(points  );
	for(auto const& lines   :assets.vector_database.line_data   ) draw_vector_data(lines   );
	for(auto const& polygons:assets.vector_database.polygon_data) draw_vector_data(polygons);
	}
	//route
	assets.vector_data_shader->bind();
	assets.vector_data_shader->uniform("color", ci::ColorAf::white());
	for(auto i=0u; i+1<this->user_interface.route_steps.size(); ++i){
		auto const start =get_position(this->assets.vector_database[this->user_interface.route_steps[i  ]])/pi*180.;
		auto const target=get_position(this->assets.vector_database[this->user_interface.route_steps[i+1]])/pi*180.;
		auto route=assets.router.get_fastest_route(start, target).on_error(godefv::return_status_t::print{std::cerr});
		gl::draw(ci::PolyLine2d{assets.router.get_points(route)});
	}
	//end
	gl::popMatrices();
	this->user_interface.graph->propagateDraw();
}

}
