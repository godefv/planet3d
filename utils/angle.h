#pragma once

#include<concepts>
#include<numbers>

auto project_angle(std::floating_point auto x, std::floating_point auto min, std::floating_point auto max){
	using std::numbers::pi;
	auto result=x;
	while(result>max) result-=2*pi;
	while(result<min) result+=2*pi;
	return result;
}

bool is_angle_in_range(std::floating_point auto x, std::floating_point auto min, std::floating_point auto max){
	using std::numbers::pi;
	while(x<min) x+=2*pi;
	while(x>max) x-=2*pi;
	return x>=min;
}
