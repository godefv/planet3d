#pragma once

#include<string>
#include<vector>

bool replace(std::string& str, const std::string& from, const std::string& to); 
std::string to_lower(std::string const& s);
std::vector<std::string> split(std::string const& s);

