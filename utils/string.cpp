#include"string.h"

#include"../extern/utf8.h"

#include<algorithm>
#include<sstream>
#include<iterator>

bool replace(std::string& str, const std::string& from, const std::string& to){
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

std::string to_lower(std::string const& s){
	std::string result=s;
	utf8lwr(result.data());
	return result;
}

std::vector<std::string> split(std::string const& s){
	std::istringstream ss{s};
	return std::vector<std::string>{
		std::istream_iterator<std::string>{ss},
		std::istream_iterator<std::string>{}
	};
}

