#pragma once

#include"cinder/gl/gl.h"

ci::vec3 make_spherical_coordinates(ci::vec3 point);
ci::vec3 from_spherical_coordinates(ci::vec3 spherical_coordinates);
