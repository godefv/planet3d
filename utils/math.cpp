#include"math.h"

ci::vec3 make_spherical_coordinates(ci::vec3 point){
	float radius=length(point);
	point/=radius;
	float latitude=asin(point.z);
	float longitude=atan2(point.y, point.x);
	return ci::vec3(latitude, longitude, radius);
}

ci::vec3 from_spherical_coordinates(ci::vec3 spherical_coordinates){
	float latitude =spherical_coordinates.x;
	float longitude=spherical_coordinates.y;
	float cos_latitude=cos(latitude);
	return spherical_coordinates.z*ci::vec3(cos_latitude*cos(longitude), cos_latitude*sin(longitude) , sin(latitude));
}
