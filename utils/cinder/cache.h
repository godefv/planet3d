#pragma once

#include"cinder/DataSource.h"

#include<filesystem>

namespace godefv{

ci::DataSourceRef load(std::filesystem::path const& path, ci::Url const& url);

}
