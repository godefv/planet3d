#pragma once

#include"cinder/Json.h"
#include"cinder/Color.h"

#include"error_checking/exit.h"

#include<vector>

namespace godefv{

template<class T>
std::vector<T> get_array(ci::JsonTree const& node){
	std::vector<T> result;
	for(ci::JsonTree const& child:node){
		result.push_back(child.getValue<T>());
	}
	return result;
}

template<class T>
std::unordered_map<std::string,T> get_map(ci::JsonTree const& node){
	std::unordered_map<std::string,T> result;
	for(ci::JsonTree const& child:node){
		result.emplace(child.getKey(), child.getValue<T>());
	}
	return result;
}

void apply_to_child_if_exists(ci::JsonTree const& node, std::string const& child_path, auto const& function){
	if(!node.hasChild(child_path)) return;
	function(node.getChild(child_path));
}

void load_if_exists(ci::JsonTree const& config, std::string const& child_path, auto& x){
	try{
		apply_to_child_if_exists(config, child_path, [&](ci::JsonTree const& node){node>>x;});
	}catch(std::exception const& e ) {
		godefv::exit(std::string{"error loading "}+child_path+": "+e.what());
	}
}

}

namespace cinder{

template<class T> requires std::floating_point<T> || std::same_as<T, std::string>
void operator>>(ci::JsonTree const& config, T& x){
	x=config.getValue<std::decay_t<decltype(x)>>();
}

void operator>>(ci::JsonTree const& config, std::vector<auto>& x){
	x.resize(config.getNumChildren());
	for(auto i=0u; i<config.getNumChildren(); ++i){
		config.getChild(i)>>x[i];
	}
}

template<class T>
void operator>>(ci::JsonTree const& config, std::unordered_map<std::string, T>& x){
	for(auto const& child:config){
		T value; child>>value;
		x.emplace(child.getKey(), value);
	}
}

void operator>>(ci::JsonTree const& config, ci::ColorA8u& x);
void operator>>(ci::JsonTree const& config, ci::ColorAf& x);

}
