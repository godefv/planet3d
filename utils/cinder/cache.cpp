#include"cache.h"

namespace godefv{

ci::DataSourceRef load(std::filesystem::path const& path, ci::Url const& url){
	std::error_code error;
	if(std::filesystem::exists(path, error)){
		return ci::loadFile(path.c_str());
	}
	if(!url.str().empty()){
		auto result=ci::loadUrl(url);
		ci::writeFileStream(path.c_str())->write(*result->getBuffer());
		return result;
	}
	throw std::runtime_error{std::string{}+"file not found at "+path.c_str()+"and no url is set for downloading."};
}

}
