#include"json.h"

namespace cinder{

void operator>>(ci::JsonTree const& config, ci::ColorA8u& x){
	auto color_string=config.getValue();
	if(color_string[0]=='#') color_string=color_string.substr(1);
	uint32_t color_hex; std::istringstream{color_string}>>std::hex>>color_hex;
	x=ci::ColorA8u::hex(color_hex);
}
void operator>>(ci::JsonTree const& config, ci::ColorAf& x){
	ci::ColorA8u color8u; config>>color8u;
	x=color8u;
}

}
