#pragma once

#include"cinder/Surface.h"

template<class T>
void clear(ci::ChannelT<T>* image){
	for(auto i=0u; i<image->getWidth() ; ++i){
	for(auto j=0u; j<image->getHeight(); ++j){
		image->setValue(ci::ivec2(i,j), 0);
	}}
}

template<class T>
void clear(ci::SurfaceT<T>* image){
	for(auto i=0u; i<image->getWidth() ; ++i){
	for(auto j=0u; j<image->getHeight(); ++j){
		image->setPixel(ci::ivec2(i,j), ci::ColorT<T>{});
	}}
}


