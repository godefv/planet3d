#ifndef ASSETS_H
#define ASSETS_H 

#include"planet/render_data.h"
#include"planet/data_vector/database.h"
#include"planet/data_vector/routing.h"
#include"planet/data_raster/geotiff.h"

#include"cinder/gl/gl.h"
#include"cinder/Json.h"

namespace godefv::planet3d{

struct assets_t{
	planet_render_data_t planet;

	vector_database_t vector_database;
	router_t router;
	ci::gl::GlslProgRef vector_data_shader;

	ci::gl::BatchRef stars;
	ci::gl::Texture2dRef stars_texture;
};

assets_t load_assets();

}

#endif /* ASSETS_H */
