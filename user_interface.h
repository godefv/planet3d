#pragma once

#include"assets.h"
#include"planet/data_vector/database.h"

#include "vu/View.h"
#include"vu/vu.h"
#include"vu/Control.h"

#include<vector>

namespace godefv::planet3d{

struct user_interface_t{
	vu::GraphRef graph;
	vu::ViewRef search_panel;
	vu::TextFieldRef search_field;
	vu::VSelectorRef search_results_ui;
	vu::VSelectorRef route_steps_ui;
	vu::ButtonRef add_route_step_button;
	std::vector<vector_database_index_t> search_results;
	std::vector<vector_database_index_t> route_steps;
	void setup(assets_t const& assets, ci::dvec3* spectator_position);
};

}

