#version 150

#define pi 3.14159265358979323846264338327

uniform sampler2DArray color_texture_array;
uniform usampler2D color_texture_array_lookup;
uniform ivec2 color_texture_id_offset;
uniform uvec2 color_texture_count;

in vec2 color_texture_id;
in vec2 color_texture_coordinates;

uniform usampler2DArray elevation_texture_array;
uniform usampler2D elevation_texture_array_lookup;
uniform ivec2 elevation_texture_id_offset;
uniform uvec2 elevation_texture_count;

uniform vec3 view_coordinates;
in vec3 spherical_coordinates;

out vec4 	oColor;

uint fetch_elevation_texture(vec2 texture_id, vec2 texture_coordinates){
	while(texture_coordinates.x<0){texture_coordinates.x+=1.0; texture_id.y-=1;}
	while(texture_coordinates.x>1){texture_coordinates.x-=1.0; texture_id.y+=1;}
	while(texture_coordinates.y<0){texture_coordinates.y+=1.0; texture_id.x+=1;}
	while(texture_coordinates.y>1){texture_coordinates.y-=1.0; texture_id.x-=1;}
	uint texture_array_id=texelFetch(elevation_texture_array_lookup, (ivec2(texture_id)-elevation_texture_id_offset+ivec2(elevation_texture_count))%ivec2(elevation_texture_count), 0).r;
	return texture(elevation_texture_array, vec3(texture_coordinates.xy, texture_array_id)).r;
}

vec4 fetch_color_texture(vec2 texture_id, vec2 texture_coordinates){
	while(texture_coordinates.x<0){texture_coordinates.x+=1.0; texture_id.y-=1;}
	while(texture_coordinates.x>1){texture_coordinates.x-=1.0; texture_id.y+=1;}
	while(texture_coordinates.y<0){texture_coordinates.y+=1.0; texture_id.x+=1;}
	while(texture_coordinates.y>1){texture_coordinates.y-=1.0; texture_id.x-=1;}
	uint texture_array_id=texelFetch(color_texture_array_lookup, (ivec2(texture_id)-color_texture_id_offset+ivec2(color_texture_count))%ivec2(color_texture_count), 0).r;
	return texture(color_texture_array, vec3(texture_coordinates.xy, texture_array_id));
}

mat3 make_texture_frame(vec3 spherical_coordinates){
	float latitude =spherical_coordinates.x;
	float longitude=spherical_coordinates.y;
	float cos_latitude=cos(latitude);
	float sin_latitude=sin(latitude);
	float cos_longitude=cos(longitude);
	float sin_longitude=sin(longitude);
	vec2 normal_xy=vec2(cos_longitude, sin_longitude);
	vec3 normal=vec3(cos_latitude*normal_xy, sin_latitude);
	vec3 tangent=vec3(-sin_longitude, cos_longitude, 0);
	vec3 bitangent=vec3(-sin_latitude*normal_xy, cos_latitude);
	return mat3(tangent, bitangent, normal);
}
vec3 make_3d_position(vec3 spherical_coordinates){
	float latitude =spherical_coordinates.x;
	float longitude=spherical_coordinates.y;
	float cos_latitude=cos(latitude);
	return spherical_coordinates.z*vec3(cos_latitude*cos(longitude), cos_latitude*sin(longitude) , sin(latitude));
}

vec2 ParallaxMapping(vec2 texture_id, vec2 texture_coordinates, vec3 viewDir){
    // number of depth layers
    const float minLayers = 8;
    const float maxLayers = 32;
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));
	numLayers=10;
    // calculate the size of each layer
    float layerDepth = 5000.0 / numLayers;
    // depth of current layer
    float currentLayerDepth = 0.0;
    // the amount to shift the texture coordinates per layer (from vector P)
    vec2 deltaTexCoords = viewDir.xy * (layerDepth/viewDir.z);

    // get initial values
    vec2  currentTexCoords     = texture_coordinates;
    float currentDepthMapValue = fetch_elevation_texture(texture_id, currentTexCoords);

    while(currentLayerDepth < currentDepthMapValue){
        // shift texture coordinates along direction of P
        currentTexCoords += deltaTexCoords;
        // get depthmap value at current texture coordinates
        currentDepthMapValue = fetch_elevation_texture(texture_id, currentTexCoords);
        // get depth of next layer
        currentLayerDepth += layerDepth;
    }

    // get texture coordinates before collision (reverse operations)
    vec2 prevTexCoords = currentTexCoords - deltaTexCoords;

    // get depth after and before collision for linear interpolation
    float afterDepth  = currentLayerDepth-currentDepthMapValue;
    float beforeDepth = fetch_elevation_texture(texture_id, prevTexCoords) - (currentLayerDepth-layerDepth);

    // interpolation of texture coordinates
    float weight = afterDepth / (afterDepth + beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
}

void main(){
	//elevation
	vec2 global_elevation_coordinates=vec2(spherical_coordinates.xy*180/pi)+vec2(elevation_texture_count)/2;
	ivec2 elevation_texture_id=ivec2(global_elevation_coordinates);
	vec2 elevation_texture_coordinates=global_elevation_coordinates-vec2(elevation_texture_id);
	elevation_texture_coordinates=vec2(elevation_texture_coordinates.y, 1-elevation_texture_coordinates.x);
	uint elevation_texture_array_id=texelFetch(elevation_texture_array_lookup, (ivec2(elevation_texture_id)-elevation_texture_id_offset+ivec2(elevation_texture_count))%ivec2(elevation_texture_count), 0).r;

	//color
	vec2 diff=vec2(0);
	if(elevation_texture_array_id>0u){
		vec3 view_position_relative=make_3d_position(vec3(view_coordinates.xy-spherical_coordinates.xy, view_coordinates.z+spherical_coordinates.z))-vec3(spherical_coordinates.z, 0, 0);
		vec3 view_dir=normalize(view_position_relative);
		//change frame from altitude:x to altitude:z
		view_dir=vec3(view_dir.y, -view_dir.z, view_dir.x);
		//scale to [0,1]x[0,1]xmeters
		view_dir.xy/=1.0/180*pi*spherical_coordinates.z;
		view_dir.z*=1000;
		//compute parallax occlusion mapping
		vec2 diff_aster=ParallaxMapping(elevation_texture_id, elevation_texture_coordinates,  view_dir)-elevation_texture_coordinates;
		vec2 diff_angular=diff_aster*1.0/180*pi;
		vec2 diff_mercator=diff_angular/(2*pi)*color_texture_count;
		diff=diff_mercator;
	}
	oColor=fetch_color_texture(color_texture_id, color_texture_coordinates+diff);

	//Uncomment this line to get an elevation map overlay
	//oColor.r=float(fetch_elevation_texture(elevation_texture_id, elevation_texture_coordinates))*0.01;
}

