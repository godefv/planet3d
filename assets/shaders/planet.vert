#version 150

#define pi 3.14159265358979323846264338327
#define mercator_max_latitude 1.4844222297453324

uniform mat4  ciModelViewProjection;
uniform uvec2 subdivision_count; 
uniform uvec2 color_texture_count; 
uniform float radius; 

in vec4 ciPosition;

out vec2 color_texture_id;
out vec2 color_texture_coordinates;
out vec3 spherical_coordinates;

float mercator(float latitude){
	return log(tan(pi/4+latitude/2));
}

vec4 make_3d_position(vec3 spherical_coordinates){
	float latitude =spherical_coordinates.x;
	float longitude=spherical_coordinates.y;
	float cos_latitude=cos(latitude);
	return vec4(spherical_coordinates.z*vec3(cos_latitude*cos(longitude), cos_latitude*sin(longitude) , sin(latitude)), 1);
}

vec2 make_texture_coordinates(ivec2 position_ids){
	return (vec2(position_ids)+vec2(subdivision_count)/2)/vec2(subdivision_count)*vec2(color_texture_count);
}

vec2 make_texture_coordinates_mercator(float latitude, float longitude){
	return (vec2(mercator(latitude), longitude)+pi)/(2*pi)*color_texture_count;
}

void main(){
	spherical_coordinates=vec3(ciPosition.xy, radius);
	vec4 model_vertex=make_3d_position(spherical_coordinates);
	gl_Position=ciModelViewProjection*model_vertex;
	
	//xy are the coordinates of the vertex, zw are the increment to go to the center of the quad
	color_texture_id         =make_texture_coordinates_mercator(ciPosition.x+ciPosition.z, ciPosition.y+ciPosition.w);
	color_texture_coordinates=make_texture_coordinates_mercator(ciPosition.x, ciPosition.y)-floor(color_texture_id);
	if(ciPosition.x>=mercator_max_latitude){
		color_texture_coordinates.x=1;
		color_texture_id.x=color_texture_count.x-1u;
	}else if(ciPosition.x<=-mercator_max_latitude){
		color_texture_coordinates.x=0;
		color_texture_id.x=0u;
	}
	//latitude goes first while x texture coordinate goes first
	color_texture_coordinates=vec2(color_texture_coordinates.y, color_texture_coordinates.x);
	//latitude goes north while y texture coordinate goes south
	color_texture_coordinates.y=1-color_texture_coordinates.y;
}
