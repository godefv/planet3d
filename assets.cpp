#include"assets.h"

#include"utils/cinder/json.h"
#include"utils/cinder/cache.h"
#include"error_checking/exit.h"

#include"cinder/app/App.h"
#include"cinder/Json.h"

#include<concepts>
#include<ios>
#include<sstream>
#include<ranges>

namespace cinder::gl{

void operator>>(ci::JsonTree const& config, gl::Texture2dRef& x){
	std::string url; godefv::load_if_exists(config, "url", url);
	auto const path=std::filesystem::path{ci::app::getAssetDirectories()[0].c_str()}/config.getValueForKey("path");
	x=gl::Texture2d::create(ci::loadImage(godefv::load(path, ci::Url{url})));
}

}

namespace godefv::planet3d{
namespace gl=ci::gl;

void operator>>(ci::JsonTree const& config, texture_cache_t& x){
	auto texture_source=texture_source::load(
		config.getValueForKey("source"), 
		config.getValueForKey("path")
	).on_error(return_status_t::throw_{}); 
	std::visit([&](auto& texture_source){
		load_if_exists(config, "url", texture_source.url_format);
	}, texture_source.variant);
	x=texture_cache_t{texture_source, config.getValueForKey<int>("cache_size")};
}

void operator>>(ci::JsonTree const& config, router_t& x){
	x=router_t{config.getValueForKey("database")};
}

assets_t load_assets(){
	assets_t assets;
	auto config=[](){
		try{
			return ci::JsonTree{ci::loadFile("config.json")};
		}catch(std::exception const& e ) {
			godefv::exit(std::string{"error loading config.json: "}+e.what());
		}
	}();
	load_if_exists(config, "planet_radius", assets.planet.radius);

	//planet textures
	load_if_exists(config, "planet_textures"     , assets.planet.textures     );
	load_if_exists(config, "planet_elevation_map", assets.planet.elevation_map);

	//vector data
	assets.vector_data_shader=gl::GlslProg::create(gl::GlslProg::Format{}
		.vertex  (ci::app::loadAsset("shaders/planet.vert")) 
		.fragment(ci::app::loadAsset("shaders/basic.frag"))
	);
	assets.vector_data_shader->uniform("radius", assets.planet.radius+0.1f);
	assets.vector_database=load_vector_database(config);
	load_if_exists(config, "osmscout", assets.router);

	//stars (from https://svs.gsfc.nasa.gov/4851#)
	load_if_exists(config, "stars_texture", assets.stars_texture);
	assets.stars =gl::Batch::create(ci::geom::Sphere().radius(100000.f).subdivisions(32 ), gl::getStockShader(gl::ShaderDef().texture()));

	return assets;
}

}

