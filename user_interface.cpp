#include"user_interface.h"
#include "vu/Control.h"

namespace godefv::planet3d{

void user_interface_t::setup(assets_t const& assets, ci::dvec3* spectator_position){
	vu::TextManager::instance()->setSupportedChars("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890éÉúÚíÍóÓáÁêÊûÛîÎôÔâÂèÈùÙìÌòÒàÀëËüÜïÏöÖäÄñÑœŒæÆçÇßẞ");
	this->graph=std::make_shared<vu::Graph>();
	this->graph->setFillParentEnabled();
	this->graph->connectEvents();

	auto vertical_layout=std::make_shared<vu::VerticalLayout>();
	vertical_layout->setPadding(10);
	vertical_layout->setMargin(10);

	search_panel=std::make_shared<vu::View>();
	search_panel->setLayout(vertical_layout);
	search_panel->setPos(ci::vec2());
	search_panel->setSize(ci::vec2(300,600));

    search_field=std::make_shared<vu::TextField>();
	search_field->setPlaceholderText("Search...");
	search_field->setSize(ci::vec2(250,30));
	search_field->setTextColor(ci::Color(1,1,1), vu::TextField::State::NORMAL);
	search_field->setBorderColor(ci::Color(1,1,1), vu::TextField::State::NORMAL);

	search_results_ui=std::make_shared<vu::VSelector>(ci::Rectf());
	search_results_ui->setSize(ci::vec2(250,300));
	search_results_ui->getBackground()->setColor(ci::ColorA::gray(0, 0.3f));
	search_results_ui->setHidden(true);

	route_steps_ui=std::make_shared<vu::VSelector>();
	route_steps_ui->setSize(ci::vec2(250,200));
	route_steps_ui->getBackground()->setColor(ci::ColorA::gray(0, 0.3f));
	route_steps_ui->setHidden(true);

	add_route_step_button=std::make_shared<vu::Button>();
	add_route_step_button->setTitle("Add route step");
	add_route_step_button->setSize(ci::vec2(250,30));

	//connect events
	search_results_ui->getSignalValueChanged().connect([&, out_pos=spectator_position](){
		auto selected_index=search_results_ui->getSelectedIndex();
		if(selected_index<0 || selected_index>=this->search_results.size()) return;
		auto const index=this->search_results[selected_index];
		auto const position=get_position(assets.vector_database[index]);
		out_pos->x=position.x;
		out_pos->y=position.y;
	});
	
	search_field->getSignalTextInputCompleted().connect([&](){
		this->search_results=assets.vector_database.search(search_field->getText());
		search_results_ui->getSegmentLabels().clear();
		for(auto index:this->search_results){
			auto const result_variant=assets.vector_database[index];
			std::visit([&](auto const& result){
				search_results_ui->getSegmentLabels().push_back(result.name);
			}, result_variant);
		}
		search_results_ui->getSegmentLabels().push_back("");
		search_results_ui->select(this->search_results.size());
		search_results_ui->setHidden(false);
	});

	add_route_step_button->getSignalReleased().connect([&](){
		auto selected_index=search_results_ui->getSelectedIndex();
		if(selected_index<0 || selected_index>=this->search_results.size()) return;
		//Update route
		auto const index=this->search_results[selected_index];
		route_steps.push_back(index);
		//Update UI
		auto const result_variant=assets.vector_database[index];
		std::visit([&](auto const& result){
			route_steps_ui->getSegmentLabels().push_back(result.name);
		}, result_variant);
		route_steps_ui->setHidden(false);
	});

	this->graph->addSubview(search_panel);
	search_panel->addSubview(search_field);
	search_panel->addSubview(search_results_ui);
	search_panel->addSubview(add_route_step_button);
	search_panel->addSubview(route_steps_ui);
}

}
