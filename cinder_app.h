#pragma once

#include"assets.h"
#include"user_interface.h"

#include"cinder/app/App.h"
#include"cinder/CameraUi.h"

namespace godefv::planet3d{

struct cinder_app:public ci::app::App{
	void setup() override;
	void draw() override;
	void update() override;
	void mouseDown(ci::app::MouseEvent event) override;
	void mouseDrag(ci::app::MouseEvent event) override;
	void mouseWheel(ci::app::MouseEvent event) override;
	void keyDown(ci::app::KeyEvent key) override;
	void resize() override;
	~cinder_app() override=default;

	//parameters
	ci::ivec2 reference_window_size=ci::ivec2(640, 480);
	float reference_fov=35;
	double drag_speed=0.0045/15000.0;
	double altitude_for_texture_level_0=70000.0;
	//data
	assets_t assets;
	//runtime
	//game_state_t game_state;
	bool is_running=true;
	int zoom_level;
	ci::vec2 view_size;
	//- spectator
	ci::dvec3 spectator_position={0.0,0.0,15000.0};
	double spectator_roll;
	bool spectator_position_has_changed=true;
	ci::CameraPersp	spectator_camera;
	//- user interface
	user_interface_t user_interface;
	ci::ivec2 last_mouse_position;
};

}
